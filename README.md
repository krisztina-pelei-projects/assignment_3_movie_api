# Movie Characters API

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Web API and database** with **Spring**.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This is a web API containing Movies, Movie Characters and Movie Franchises as entities. The entity relations are as follows:
- *one* **movie** can belong to *one* **franchise**, and *one* **franchise** can contain *many* **movies**
- *one* **movie** can contain *many* **characters**, and *one* **character** can appear in *many* **movies**

## API endpoints

- GET api/movies – get all movies
- GET api/movies/:id – get movie by ID
- GET api/movies/:id/characters – get all characters in movie
- POST api/movies – add new movie
- PUT api/movies/:id  – update movie
- DELETE api/movies/:id  – delete movie
- GET api/franchises – get all franchises
- GET api/franchises/:id – get franchise by ID
- GET api/franchises/:id/movies – get all movies in a franchise
- GET api/franchises/:id/characters – get all characters in a franchise
- POST api/franchises – add new franchise
- PUT api/franchises/:id  – update franchise
- PUT api/franchises/:id/movies – update all movies in a franchise
- DELETE api/franchises/:id  – delete franchise
- GET api/characters – get all characters
- GET api/characters/:id – get character by ID
- POST api/characters – add new character
- PUT api/characters/:id  – update character
- DELETE api/characters/:id  – delete character


## Install

- Install JDK 17
- Install Intellij
- Clone repository

## Usage

Runnig the application locally, the API is available at http://localhost:8080

The application is deployed via Heroku and can be accessed at https://movie-api-assignment-3.herokuapp.com/

## Maintainers

- [Laszlo Laki](https://gitlab.com/lakilukelaszlo)
- [Adam Olah](https://gitlab.com/adam-olah93)
- [Krisztina Pelei](https://gitlab.com/kokriszti)
