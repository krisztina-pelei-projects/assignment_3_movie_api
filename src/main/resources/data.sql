-- Professors
-- INSERT INTO franchise (description, franchise_name ) VALUES ('The Marvel Universe is a fictional shared universe where the stories in most American comic book titles and other media published by Marvel Comics take place. ', 'Marvel Universe'); -- 1
-- INSERT INTO franchise (description, franchise_name ) VALUES ('The DC Universe (DCU) is the fictional shared universe where most stories in American comic book titles published by DC Comics take place.', 'DC Universe'); -- 2
-- INSERT INTO franchise (description, franchise_name ) VALUES ('The Lord of the Rings franchise is a series of', 'Lord Of The Rings Franchise is collection of epic fantasy adventure films based on the novels written by J. R. R. Tolkien.'); -- 3

INSERT INTO movie (movie_title) VALUES ('Matrix');
INSERT INTO movie (movie_title, release_year) VALUES ('Matrix 2', 2004);
INSERT INTO movie (movie_title) VALUES ('Matrix 3');
INSERT INTO movie (movie_title) VALUES ('Iron Man');
INSERT INTO movie (movie_title) VALUES ('Iron Man 2');
INSERT INTO movie (movie_title) VALUES ('Avengers');
INSERT INTO movie (movie_title) VALUES ('Avengers 2');
INSERT INTO movie (movie_title) VALUES ('Harry Potter and the Deathly Hallows');
INSERT INTO movie (movie_title) VALUES ('Harry Potter and the Prisoner of Azkaban');

INSERT INTO franchise (franchise_name) VALUES ('Avengers');
INSERT INTO franchise (franchise_name) VALUES ('Matrix');
INSERT INTO franchise (franchise_name, description) VALUES ('Harry Potter', 'Fantasy word of wizards and witches');

INSERT INTO character (char_name, alias, gender) VALUES ('Tony Stark', 'Iron Man', 'male');
INSERT INTO character (char_name, alias, gender) VALUES ('Steve Rogers', 'Captain Amerika', 'male');
INSERT INTO character (char_name, alias, gender) VALUES ('Thomas Anderson', 'Neo', 'male');
INSERT INTO character (char_name, gender) VALUES ('Harry Potter', 'male');
INSERT INTO character (char_name, gender) VALUES ('Hermione Granger', 'female');

UPDATE movie SET franchise_id = 1 WHERE movie_id = 4;
UPDATE movie SET franchise_id = 1 WHERE movie_id = 5;
UPDATE movie SET franchise_id = 1 WHERE movie_id = 6;
UPDATE movie SET franchise_id = 1 WHERE movie_id = 7;
UPDATE movie SET franchise_id = 2 WHERE movie_id = 1;
UPDATE movie SET franchise_id = 2 WHERE movie_id = 2;
UPDATE movie SET franchise_id = 2 WHERE movie_id = 3;
UPDATE movie SET franchise_id = 3 WHERE movie_id = 8;
UPDATE movie SET franchise_id = 3 WHERE movie_id = 9;

INSERT INTO movie_character (movie_id, character_id) VALUES (1, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (2, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (3, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (4, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (5, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (6, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (7, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (8, 4);
INSERT INTO movie_character (movie_id, character_id) VALUES (8, 5);
INSERT INTO movie_character (movie_id, character_id) VALUES (9, 4);
INSERT INTO movie_character (movie_id, character_id) VALUES (6, 2);
INSERT INTO movie_character (movie_id, character_id) VALUES (7, 2);

