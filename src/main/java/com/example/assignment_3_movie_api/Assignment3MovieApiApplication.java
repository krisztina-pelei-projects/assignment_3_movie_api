package com.example.assignment_3_movie_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3MovieApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment3MovieApiApplication.class, args);
	}

}
