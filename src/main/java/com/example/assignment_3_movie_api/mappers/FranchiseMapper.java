package com.example.assignment_3_movie_api.mappers;

import com.example.assignment_3_movie_api.models.DTOs.FranchiseDTO;
import com.example.assignment_3_movie_api.models.DTOs.MovieDTO;
import com.example.assignment_3_movie_api.models.Franchise;
import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
     protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);

    @Mapping(target = "movies",source = "movies",qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoFranchise(FranchiseDTO dto);

    //Custom mappings

    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Integer> id) {
        if (id == null) {
            return new HashSet<>();
        }
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(m -> m.getId()).collect(Collectors.toSet());
    }

}


