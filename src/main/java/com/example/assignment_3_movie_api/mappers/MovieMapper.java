package com.example.assignment_3_movie_api.mappers;

import com.example.assignment_3_movie_api.models.DTOs.MovieDTO;
import com.example.assignment_3_movie_api.models.Franchise;
import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.services.MovieCharacterImpl;
import com.example.assignment_3_movie_api.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected FranchiseService franchiseService;

    @Autowired
    protected MovieCharacterImpl characterService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    // Mapping the other way
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);

    //Mapping collection, implemented automatically
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<MovieCharacter> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(character -> character.getId()).collect(Collectors.toSet());
    }

    @Named("characterIdsToCharacters")
    Set<MovieCharacter> mapIdsToCharacters(Set<Integer> characterIds) {
        if (characterIds == null) {
            return new HashSet<>();
        }
        return characterIds.stream()
                .map( id -> characterService.findById(id))
                .collect(Collectors.toSet());
    }

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(Integer id) {
        if(id != null) {
            return franchiseService.findById(id);
        }
        return null;
    }
}
