package com.example.assignment_3_movie_api.mappers;

import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.services.MovieService;
import com.example.assignment_3_movie_api.models.DTOs.MovieCharacterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieCharacterMapper {

    @Autowired
    protected MovieService movieService;

    // This method map MovieCharacter to MovieCharacterDTO
    @Mapping(target="movies",source="movies",qualifiedByName="moviesToIds")
    public abstract MovieCharacterDTO movieCharacterToMovieCharacterDTO(MovieCharacter character);



    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    // This method map MovieCharacterDTO to MovieCharacter
    @Mapping(target="movies",source="movies",qualifiedByName="movieIdsToMovies")
    public abstract MovieCharacter movieCharacterDTOToMovieCharacter(MovieCharacterDTO characterDTO);

    @Named("movieIdsToMovies")
    Set<Movie> movieIDsToMovies(Set<Integer> id) {
        if (id == null) {
            return new HashSet<>();
        }
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    // Collection<MovieCharacter> to Collection<MovieCharacterDTO>
    public abstract Collection<MovieCharacterDTO> movieCharacterToMovieCharacterDTO(Collection<MovieCharacter> characters);

}
