package com.example.assignment_3_movie_api.services;

import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.models.MovieCharacter;

import java.util.Collection;
import java.util.Set;

public interface MovieService extends CRUDService<Movie, Integer>{
    public Collection<MovieCharacter> findAllCharactersInMovie(Integer id);

    public void updateAllCharactersInMovie(Set<Integer> characters, Integer id);
}
