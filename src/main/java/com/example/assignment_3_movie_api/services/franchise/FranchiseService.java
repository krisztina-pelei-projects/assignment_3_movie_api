package com.example.assignment_3_movie_api.services.franchise;

import com.example.assignment_3_movie_api.models.Franchise;
import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.services.CRUDService;

import java.util.Collection;
import java.util.Set;

public interface FranchiseService extends CRUDService<Franchise, Integer> {
    public Collection<Movie> findAllMoviesInFranchise(Integer id);

    public void updateAllMoviesInFranchise(Set<Integer> movies, Integer id);

    public Collection<MovieCharacter> findAllCharactersInFranchise(Integer id);
}
