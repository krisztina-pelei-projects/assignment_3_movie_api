package com.example.assignment_3_movie_api.services.franchise;

import com.example.assignment_3_movie_api.exceptions.DocumentNotFoundException;
import com.example.assignment_3_movie_api.exceptions.FranchiseNotFoundException;
import com.example.assignment_3_movie_api.models.Franchise;
import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.repositories.FranchiseRepository;
import com.example.assignment_3_movie_api.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class FranchiseServiceImpl implements FranchiseService{
    //Attributes
    private final Logger logger = LoggerFactory.getLogger(FranchiseServiceImpl.class);
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    //Constructor
    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    //Methods
    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save((entity));
    }

    @Override
    public Franchise update(Franchise updateFranchise) {
        return franchiseRepository.save(updateFranchise);
    }

    @Override
    public void deleteById(Integer id) {
        if(franchiseRepository.existsById(id)) {
            Franchise franchiseFromDB = franchiseRepository.findById(id).get();
            franchiseFromDB.getMovies().forEach(movie -> movie.setFranchise(null));
            franchiseRepository.deleteById(id);
        }
        else
            throw new FranchiseNotFoundException(id);
            logger.warn("No Franchise exists with ID: " + id);
    }


    @Override
    public void delete(Franchise entity) {

    }

    @Override
    public Collection<Movie> findAllMoviesInFranchise(Integer id) {
        Franchise franchiseFromDB = franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
        return franchiseFromDB.getMovies();
    }

    @Override
    public void updateAllMoviesInFranchise(Set<Integer> movieIDs, Integer id) {
        Franchise franchiseFromDB = franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
        franchiseFromDB.getMovies().forEach(movie -> movie.setFranchise(null));
        movieIDs.forEach(movieID -> movieRepository.findById(movieID).get().setFranchise(franchiseFromDB));
    }

    @Override
    public Collection<MovieCharacter> findAllCharactersInFranchise(Integer id) {
        Franchise franchiseFromDB = franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));

        Collection<Movie> moviesInFranchise = franchiseFromDB.getMovies();
        moviesInFranchise.forEach(movie -> movieRepository.findById(movie.getId()).get().getCharacters());
        Set<MovieCharacter> charactersInFranchise = new HashSet<>();
        moviesInFranchise.forEach(movie -> {
            movieRepository.findById(movie.getId()).get().getCharacters().forEach( character -> charactersInFranchise.add(character));
        });
        return charactersInFranchise;
    }


}
