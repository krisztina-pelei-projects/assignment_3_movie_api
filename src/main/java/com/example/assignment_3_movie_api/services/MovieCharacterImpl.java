package com.example.assignment_3_movie_api.services;

import com.example.assignment_3_movie_api.exceptions.DocumentNotFoundException;
import com.example.assignment_3_movie_api.exceptions.MovieCharacterNotFoundException;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.repositories.MovieCharacterRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class MovieCharacterImpl implements MovieCharacterService {

    private final MovieCharacterRepository movieCharacterRepository;

    public MovieCharacterImpl (MovieCharacterRepository movieCharacterRepository){
        this.movieCharacterRepository = movieCharacterRepository;
    }

    @Override
    public MovieCharacter findById(Integer id) {
        return movieCharacterRepository.findById(id).orElseThrow(() -> new MovieCharacterNotFoundException(id));
    }

    @Override
    public Collection<MovieCharacter> findAll() {
        return movieCharacterRepository.findAll();
    }

    @Override
    public MovieCharacter add(MovieCharacter character) {
        return movieCharacterRepository.save(character);
    }

    @Override
    public MovieCharacter update(MovieCharacter character) {
            return movieCharacterRepository.save(character);
    }

    @Override
    public void deleteById(Integer id) {
        MovieCharacter characterFromDB = movieCharacterRepository.findById(id)
                .orElseThrow(() -> new MovieCharacterNotFoundException(id));
        characterFromDB.getMovies().forEach(movie -> movie.getCharacters().remove(characterFromDB));
        movieCharacterRepository.deleteById(id);
    }

    @Override
    public void delete(MovieCharacter character) {
        MovieCharacter characterFromDB = movieCharacterRepository.findById(character.getId())
                .orElseThrow(() -> new MovieCharacterNotFoundException(character.getId()));
        characterFromDB.getMovies().forEach(movie -> movie.getCharacters().remove(characterFromDB));
        movieCharacterRepository.delete(character);
    }
}
