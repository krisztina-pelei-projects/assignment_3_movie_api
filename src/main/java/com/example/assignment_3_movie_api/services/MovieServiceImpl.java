package com.example.assignment_3_movie_api.services;

import com.example.assignment_3_movie_api.exceptions.DocumentNotFoundException;
import com.example.assignment_3_movie_api.exceptions.MovieNotFoundException;
import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.repositories.MovieCharacterRepository;
import com.example.assignment_3_movie_api.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;
    private final MovieCharacterRepository characterRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

    public MovieServiceImpl(MovieRepository movieRepository, MovieCharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie toSave) {
        return movieRepository.save(toSave);
    }

    @Override
    public Movie update(Movie updateMovie) {
        return movieRepository.save(updateMovie);
    }

    @Override
    public void deleteById(Integer id) {
        if(movieRepository.existsById(id)) {
            movieRepository.deleteById(id);
        } else
            throw new MovieNotFoundException(id);
    }

    @Override
    public void delete(Movie entity) {

    }

    @Override
    public Collection<MovieCharacter> findAllCharactersInMovie(Integer id) {
        Movie movieFromDB = movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));

        return movieFromDB.getCharacters();
    }

    @Override
    public void updateAllCharactersInMovie(Set<Integer> characterIDs, Integer id) {
        Movie movieFromDB = movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));

        //empty movie's character set
        movieFromDB.setCharacters(new HashSet<>());

        //loop through chars from parameter, add each character to movie's character set
        characterIDs.forEach(characterID -> {
            MovieCharacter charFromDB = characterRepository.findById(characterID).get();
            movieFromDB.getCharacters().add(charFromDB);
        });
    }
}
