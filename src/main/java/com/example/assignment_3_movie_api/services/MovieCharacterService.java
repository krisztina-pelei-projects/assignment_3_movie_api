package com.example.assignment_3_movie_api.services;

import com.example.assignment_3_movie_api.models.MovieCharacter;

public interface MovieCharacterService extends CRUDService<MovieCharacter,Integer>{

}
