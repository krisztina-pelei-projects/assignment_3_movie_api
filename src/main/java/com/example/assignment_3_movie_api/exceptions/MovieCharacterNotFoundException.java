package com.example.assignment_3_movie_api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MovieCharacterNotFoundException extends RuntimeException {
    public MovieCharacterNotFoundException(int id){
        super("Movie character with " + id + "does not exist");
    }
}
