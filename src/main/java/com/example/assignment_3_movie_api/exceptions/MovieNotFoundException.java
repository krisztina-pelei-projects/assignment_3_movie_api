package com.example.assignment_3_movie_api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MovieNotFoundException extends RuntimeException {
        public MovieNotFoundException(int id){
            super("Movie with " + id + "does not exist");
        }
}
