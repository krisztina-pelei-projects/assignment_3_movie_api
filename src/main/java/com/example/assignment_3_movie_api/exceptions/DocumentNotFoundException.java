package com.example.assignment_3_movie_api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DocumentNotFoundException extends RuntimeException{
    public DocumentNotFoundException(int id) {
        super("No document found with ID " + id);
    }
}
