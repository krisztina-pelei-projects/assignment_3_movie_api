package com.example.assignment_3_movie_api.repositories;

import com.example.assignment_3_movie_api.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieCharacterRepository extends JpaRepository <MovieCharacter, Integer> {
}
