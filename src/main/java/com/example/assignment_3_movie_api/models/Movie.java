package com.example.assignment_3_movie_api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    Integer id;

    @Column(name = "movie_title", length = 100, nullable = false)
    String title;
    @Column(length = 100)
    String genre;
    Integer releaseYear;
    @Column(length = 50)
    String director;
    @Column(length = 100)
    String picture;
    @Column(length = 50)
    String trailer;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name="movie_id")},
            inverseJoinColumns = {@JoinColumn(name="character_id")}
    )
    private Set<MovieCharacter> characters = new HashSet<>();

}
