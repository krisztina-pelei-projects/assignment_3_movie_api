package com.example.assignment_3_movie_api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "character")
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "char_id")
    private Integer id;
    @Column(name = "char_name", length = 50 ,nullable = false)
    private String name;
    @Column(length = 50)
    private String alias;
    @Column(length = 10)
    private String gender;
    @Column(length = 150)
    private String photo_url;
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies = new HashSet<>();

}
