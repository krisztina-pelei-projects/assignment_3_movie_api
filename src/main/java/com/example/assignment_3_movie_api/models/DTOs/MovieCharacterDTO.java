package com.example.assignment_3_movie_api.models.DTOs;

import lombok.Data;

import java.util.Set;

@Data
public class MovieCharacterDTO {

    Integer id;
    String name;
    String alias;
    String gender;
    String photo_url;
    Set<Integer> movies;
}

