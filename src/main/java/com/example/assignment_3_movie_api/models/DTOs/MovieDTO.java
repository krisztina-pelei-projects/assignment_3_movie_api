package com.example.assignment_3_movie_api.models.DTOs;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    Integer id;
    String title;
    String genre;
    Integer releaseYear;
    String director;
    String picture;
    String trailer;
    Integer franchise;
    Set<Integer> characters;
}
