package com.example.assignment_3_movie_api.models.DTOs;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDTO {
    private Integer id;
    private String name;
    private String description;
    Set<Integer> movies;
}
