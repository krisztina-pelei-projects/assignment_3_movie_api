package com.example.assignment_3_movie_api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private Integer id;
    @Column(name = "franchise_name", length = 50, nullable = false)
    private String name;
    @Column(length = 500)
    private String description;
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies = new HashSet<>();
}
