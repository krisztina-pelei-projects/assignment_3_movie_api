package com.example.assignment_3_movie_api.controllers;

import com.example.assignment_3_movie_api.exceptions.MovieCharacterNotFoundException;
import com.example.assignment_3_movie_api.exceptions.MovieNotFoundException;
import com.example.assignment_3_movie_api.mappers.MovieCharacterMapper;
import com.example.assignment_3_movie_api.models.DTOs.FranchiseDTO;
import com.example.assignment_3_movie_api.models.DTOs.MovieCharacterDTO;
import com.example.assignment_3_movie_api.models.DTOs.MovieDTO;
import com.example.assignment_3_movie_api.models.MovieCharacter;
import com.example.assignment_3_movie_api.services.MovieCharacterService;
import com.example.assignment_3_movie_api.services.MovieService;
import com.example.assignment_3_movie_api.services.franchise.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

        @RestController
        @RequestMapping(path = "api/characters")
        public class MovieCharacterController {

            private final MovieCharacterService movieCharacterService;
            private final MovieCharacterMapper movieCharacterMapper;

            public MovieCharacterController(MovieCharacterService movieCharacterService, MovieCharacterMapper movieCharacterMapper) {
                this.movieCharacterService = movieCharacterService;
                this.movieCharacterMapper = movieCharacterMapper;
            }

            // This method retrieves all characters from DB
            @Operation(summary = "Retrieves all the characters")
            @ApiResponses(value = {
                    @ApiResponse(responseCode = "200",
                            description = "Success",
                            content = { @Content(mediaType ="application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieCharacterDTO.class)))}),
                    @ApiResponse(responseCode = "404",
                            description = "Something went wrong...try again",
                            content = { @Content(mediaType ="application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ApiErrorResponse.class)))})
            })
            @GetMapping
            public ResponseEntity findAll() {
                Collection<MovieCharacterDTO> characters = movieCharacterMapper.movieCharacterToMovieCharacterDTO(
                        movieCharacterService.findAll()
                );
                return ResponseEntity.ok(characters);
            }

            // This method retrieves a specific character by ID
            @Operation(summary = "Retrieves a specific character by ID")
            @ApiResponses(value = {
                    @ApiResponse(responseCode = "200",
                            description = "Success",
                            content = { @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = MovieCharacterDTO.class)) }),
                    @ApiResponse(responseCode = "404",
                            description = "No character associated with given ID",
                            content = { @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = MovieCharacterNotFoundException.class)) })
            })
            @GetMapping("{id}")
            public ResponseEntity findById(@PathVariable int id) {
                MovieCharacterDTO character = movieCharacterMapper.movieCharacterToMovieCharacterDTO(
                        movieCharacterService.findById(id)
                );
                return ResponseEntity.ok(character);
            }

            // This method adds a new character to DB
            @Operation(summary = "Adds a new character to DB")
            @ApiResponses(value = {
                    @ApiResponse(responseCode = "201",
                            description="Successfully added a character",
                            content = @Content ),
                    @ApiResponse(responseCode = "400",
                            description = "Malformed request",
                            content = { @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class))})
            })
            @PostMapping
            public  ResponseEntity add(@RequestBody MovieCharacter character) {
                MovieCharacter newCharacter = movieCharacterService.add(character);
                URI location = URI.create("characters/" + character.getId());
                return ResponseEntity.created(location).build();
            }

            // This method updates a character corresponding to the given ID
            @Operation(summary="Updates character corresponding to the given ID")
            @ApiResponses( value = {
                    @ApiResponse(responseCode = "204",
                            description = "Successfully updated the character",
                            content = @Content),
                    @ApiResponse(responseCode = "400",
                            description = "Malformed request",
                            content = { @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
                    @ApiResponse(responseCode = "404",
                            description = "No character associated with the given ID",
                            content = @Content)
            })
            @PutMapping("{id}")
            public ResponseEntity update(@RequestBody MovieCharacter character, @PathVariable int id) {
                if (id != character.getId())
                    return ResponseEntity.badRequest().build();
                movieCharacterService.update(character);
                return ResponseEntity.noContent().build();
            }

            // This method deletes a character corresponding to the given ID
            @Operation(summary = "Deletes a specific character by ID")
            @ApiResponses(value={
                    @ApiResponse(responseCode = "204",
                            description = "Success",
                            content = { @Content(mediaType ="application/json",
                                    schema = @Schema(implementation = MovieCharacterDTO.class))

                            }),
                    @ApiResponse(responseCode = "404",
                            description = "No character associated with the given ID",
                            content = { @Content(mediaType ="application/json",
                                    schema = @Schema(implementation = MovieCharacterNotFoundException.class))})
            })
            @DeleteMapping("{id}")
            public ResponseEntity delete(@PathVariable int id) {
                movieCharacterService.deleteById(id);
                return ResponseEntity.noContent().build();
            }
        }
