package com.example.assignment_3_movie_api.controllers;

import com.example.assignment_3_movie_api.exceptions.FranchiseNotFoundException;
import com.example.assignment_3_movie_api.exceptions.MovieCharacterNotFoundException;
import com.example.assignment_3_movie_api.mappers.FranchiseMapper;
import com.example.assignment_3_movie_api.mappers.MovieCharacterMapper;
import com.example.assignment_3_movie_api.mappers.MovieMapper;
import com.example.assignment_3_movie_api.models.DTOs.FranchiseDTO;
import com.example.assignment_3_movie_api.models.DTOs.MovieCharacterDTO;
import com.example.assignment_3_movie_api.models.DTOs.MovieDTO;
import com.example.assignment_3_movie_api.models.Franchise;
import com.example.assignment_3_movie_api.services.MovieCharacterService;
import com.example.assignment_3_movie_api.services.MovieService;
import com.example.assignment_3_movie_api.services.franchise.FranchiseService;
import com.example.assignment_3_movie_api.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/franchises")
public class FranchiseController {
    //Attributes
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final MovieCharacterMapper characterMapper;
    private final MovieCharacterService characterService;

    //Constructor
    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, MovieService movieService, MovieCharacterMapper characterMapper, MovieCharacterService characterService) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.movieService = movieService;
        this.characterMapper = characterMapper;
        this.characterService = characterService;
    }
    //Swagger Documentation
    @Operation(summary = "Find all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Something went wrong...try again",
                    content = { @Content(mediaType ="application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ApiErrorResponse.class)))})
    })
    //Method/find all franchise
    @GetMapping
    public ResponseEntity<Collection<FranchiseDTO>> findAll() {
        Collection<FranchiseDTO> franchiseDTOs = franchiseMapper.franchiseToFranchiseDto(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDTOs);
    }
    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseNotFoundException.class)) })
    })
    //Method/Find by id
    @GetMapping("{id}")
    public ResponseEntity<FranchiseDTO> findById(@PathVariable int id) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }
    @Operation(summary = "Adds new Franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<FranchiseDTO> add(@RequestBody Franchise franchiseToSave) {
        FranchiseDTO franchiseSavedDTO = franchiseMapper.franchiseToFranchiseDto(franchiseService.add(franchiseToSave));
        return new ResponseEntity<>(franchiseSavedDTO, HttpStatus.CREATED);
    }
    //Swagger documentation
    @Operation(summary = "Updates a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseNotFoundException.class)) })
    })
    //Method/ updates a franchise
    @PutMapping("{id}")
    public ResponseEntity<FranchiseDTO> update(@RequestBody FranchiseDTO updateData, @PathVariable int id) {
        // Validates if body is correct
        if(id != updateData.getId())
            return ResponseEntity.badRequest().build();
        Franchise updateFranchise = franchiseMapper.franchiseDtoFranchise(updateData);
        FranchiseDTO updatedFranchise = franchiseMapper.franchiseToFranchiseDto(franchiseService.update(updateFranchise));
        return new ResponseEntity<>(updatedFranchise, HttpStatus.OK);
    }

    @Operation(summary = "Deletes a specific franchise by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = FranchiseNotFoundException.class))})
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    //swagger documentation
    @Operation(summary = "Finds all movies in franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully found in franchise",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseNotFoundException.class))
    })})
    @GetMapping("{id}/movies")
    public ResponseEntity<Collection<MovieDTO>> findAllMoviesInFranchise(@PathVariable int id) {
        Collection<MovieDTO> movieDTOs = movieMapper.movieToMovieDto(franchiseService.findAllMoviesInFranchise(id));
        return ResponseEntity.ok(movieDTOs);
    }
    //Swagger documentation
    @Operation(summary = "Updates all movies in franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movies successfully updated in franchise",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movies not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}/movies")
    public ResponseEntity update(@RequestBody Integer[] updateData, @PathVariable int id) {
        Set<Integer> updateMovieIDs = new HashSet<>(Arrays.asList(updateData));
        franchiseService.updateAllMoviesInFranchise(updateMovieIDs, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary ="Finds all characters in a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully found characters associated with given franchise ID",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character/characters associated with the given franchise ID",
                    content = @Content)
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Collection<MovieCharacterDTO>> findAllCharactersInFranchise(@PathVariable int id) {
        Collection<MovieCharacterDTO> characterDTOS = characterMapper.movieCharacterToMovieCharacterDTO(franchiseService.findAllCharactersInFranchise(id));
        return ResponseEntity.ok(characterDTOS);
    }
}
