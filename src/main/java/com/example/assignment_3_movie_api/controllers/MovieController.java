package com.example.assignment_3_movie_api.controllers;

import com.example.assignment_3_movie_api.exceptions.DocumentNotFoundException;
import com.example.assignment_3_movie_api.exceptions.MovieNotFoundException;
import com.example.assignment_3_movie_api.mappers.MovieCharacterMapper;
import com.example.assignment_3_movie_api.mappers.MovieMapper;
import com.example.assignment_3_movie_api.models.DTOs.MovieCharacterDTO;
import com.example.assignment_3_movie_api.models.DTOs.MovieDTO;
import com.example.assignment_3_movie_api.models.Movie;
import com.example.assignment_3_movie_api.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/movies")
public class MovieController {


    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final MovieCharacterMapper characterMapper;


    public MovieController(MovieService movieService, MovieMapper movieMapper,MovieCharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper =characterMapper;
    }

    @Operation(summary = "Retrieves all the movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                          description = "Success",
                          content = { @Content(mediaType ="application/json",
                                  array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))}),
            @ApiResponse(responseCode = "404",
                          description = "Something went wrong...try again",
                           content = { @Content(mediaType ="application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ApiErrorResponse.class)))})
    })
    @GetMapping
    public ResponseEntity<Collection<MovieDTO>> findAll() {
        Collection<MovieDTO> movieDTOs = movieMapper.movieToMovieDto(movieService.findAll());
        return ResponseEntity.ok(movieDTOs);
    }

    @Operation(summary= "Retrieves a specific movie by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
            description = "Success",
                    content = { @Content(mediaType ="application/json",
            schema = @Schema(implementation = MovieDTO.class))

    }),
            @ApiResponse(responseCode = "404",
                    description = "No movie associated with the given ID",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = MovieNotFoundException.class))})
    })
    @GetMapping("{id}")
    public ResponseEntity<MovieDTO> findById(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "Adds a new movie to DB")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description="Successfully added a movie",
            content = @Content ),
            @ApiResponse(responseCode = "400",
            description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping
    public ResponseEntity<MovieDTO> add(@RequestBody Movie movieToSave) {
        MovieDTO movieSavedDTO = movieMapper.movieToMovieDto(movieService.add(movieToSave));
        return new ResponseEntity<>(movieSavedDTO, HttpStatus.CREATED);
    }

    //replaces whole obj with the one received in the body!
    //missing fields will be set to null
    @Operation(summary = "Updates a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated the movie",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No movie associated with the given ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity<MovieDTO> update(@RequestBody MovieDTO updateData, @PathVariable int id) {
        // Validates if body is correct
        if(id != updateData.getId())
            return ResponseEntity.badRequest().build();
        Movie updateMovie = movieMapper.movieDtoToMovie(updateData);
        MovieDTO updatedMovie = movieMapper.movieToMovieDto(movieService.update(updateMovie));
        return new ResponseEntity<>(updatedMovie, HttpStatus.OK);
    }

    @Operation(summary = "Deletes a specific movie by ID")
    @ApiResponses(value={
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = MovieDTO.class))

                    }),
            @ApiResponse(responseCode = "404",
                    description = "No movie associated with the given ID",
                    content = { @Content(mediaType ="application/json",
                            schema = @Schema(implementation = MovieNotFoundException.class))})
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates all characters in a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated the movie with characters",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character/characters associated with the given movie ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieNotFoundException.class))})
    })
    @PutMapping("{id}/characters")
    public ResponseEntity updateAllCharactersInMovie(@RequestBody Integer[] updateData, @PathVariable int id) {
        //array to Set of IDs
        Set<Integer> updateCharacterIDs = new HashSet<>(Arrays.asList(updateData));
        movieService.updateAllCharactersInMovie(updateCharacterIDs, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary ="Finds all characters in a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found characters associated with given movie ID",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character/characters associated with the given movie ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieNotFoundException.class))})
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Collection<MovieCharacterDTO>> findAllCharactersInMovie(@PathVariable int id) {
        Collection<MovieCharacterDTO> characterDTOS = characterMapper.movieCharacterToMovieCharacterDTO(movieService.findAllCharactersInMovie(id));
        return ResponseEntity.ok(characterDTOS);
    }
}
